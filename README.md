Latex persoPackage
==

Installation
--

Install the file with the others package :

    cp persoPackage.sty /usr/share/texmf-dist/tex/latex/persoPackage/

Update the package database :

    texhash
    

Utilisation
--

    \usepackage{persoPackage}
